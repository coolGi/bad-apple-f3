package com.coolgi.badapple;

public class ModInfo {
    public static final String ID = "badapple";
    public static final String NAME = "BadApple";
    public static final String READABLE_NAME = "Bad Apple";
    public static final String VERSION = "1.0.0";
    public static boolean IS_DEV_BUILD = VERSION.toLowerCase().contains("dev");
}
