package com.coolgi.badapple;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BadApple {
    // Get the bad apple file from https://archive.org/download/TouhouBadApple
    public static final int frameHeight = 10;
    public static final int videoTime = 219300; // Time the video takes to complete in milliseconds
    public static final String video = "badapple.txt";

    public static final int frameLength = videoTime/(countLines(accessFile(video))/frameHeight);

    public static void init() {
//        System.out.println("Inited");
    }




    /** Get a file within the mods resources */
    public static InputStream accessFile(String resource) {
        // this is the path within the jar file
        InputStream input = BadApple.class.getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = BadApple.class.getClassLoader().getResourceAsStream(resource);
        }

        return input;
    }

    // Copied from stack overflow
    // https://stackoverflow.com/questions/453018/number-of-lines-in-a-file-in-java
    public static int countLines(InputStream file) {
        InputStream is = new BufferedInputStream(file);
        try {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } catch (Exception e) { e.printStackTrace(); }
        return -1;
    }
}
