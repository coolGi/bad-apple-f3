# BadApple in Minecraft F3
#### A small gimmick mod that adds bad apple in the f3 screen

Here is an example video to show how the mod works\
[![Example video](https://img.youtube.com/vi/yDxa_kREViI/0.jpg)](https://www.youtube.com/watch?v=yDxa_kREViI)

It just starts a video as soon as you open the f3 screen and continues playing form there

### To build
1. `git clone https://gitlab.com/coolGi/bad-apple-f3.git`
2. `cd bad-apple-f3.git`
3. `./gradlew fabric:build` or `./gradlew forge:build`
4. Out put is in `fabric/build/libs` or `forge/build/libs` (its the file with the shortest name)

#### Credits
Thx to F53 for making a video to braille converter https://github.com/CodeF53/Video-To-Braille \
Thx to the original bad apple author for making bad apple

#### Note:
To change what version of minecraft to build for add the flag `-PmcVer=1.?` to the end of the command\
eg. `./gradlew faric:build -PmcVer=1.18.2` (by default it is for 1.18.2)\
Accepts `1.16.5`, `1.17.1`, `1.18.1`, `1.18.2`

# WARNING:
The code for this is terrible and was made in literally a day as a gimmick\
Tou can use it for whatever you want without any credit\
(if it crashes it isn't my fault as I didn't test it that much)
